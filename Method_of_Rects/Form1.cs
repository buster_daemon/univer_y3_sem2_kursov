﻿using System;
using System.Windows.Forms;

namespace Method_of_Rects
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            OutBox.Clear();
        }

        static double Rects(Func<double, double> f, double a, double b, int n, int type)
        {
            if (type == 0)
            {
                var h = (b - a) / n;
                var sum = 0d;
                for (var i = 0; i <= n - 1; i++)
                {
                    var x = a + i * h;
                    sum += f(x);
                }
                var result = h * sum;
                return result;
            }
            else if (type == 1)
            {
                var h = (b - a) / n;
                var sum = 0d;
                for (var i = 1; i <= n; i++)
                {
                    var x = a + i * h;
                    sum += f(x);
                }
                var result = h * sum;
                return result;
            }
            else if (type == 2)
            {
                var h = (b - a) / n;
                var sum = (f(a) + f(b)) / 2;
                for (var i = 1; i <= n; i++)
                {
                    var x = a + h * i;
                    sum += f(x);
                }
                var result = h * sum;
                return result;
            }
            return 0;
        }

        static string Solvation(int ind, double a, double b, int n, int t)
        {
            double res = 0;
            string s1 = "", s2 = "";
            if (ind == 0)
            {
                double func(double x) => Math.Sin(x);
                res = Rects(func, a, b, n, t);
                s1 = "Формула: sin(x)" + Environment.NewLine;
                s2 = "Результат: " + Convert.ToString(res) + Environment.NewLine;
            }
            else if (ind == 1)
            {
                double func(double x) => Math.Cos(x) + Math.Sin(x);
                res = Rects(func, a, b, n, t);
                s1 = "Формула: cos(x) + sin(x)" + Environment.NewLine;
                s2 = "Результат: " + Convert.ToString(res) + Environment.NewLine;
            }
            else if (ind == 2)
            {
                double func(double x) => Math.Pow(Math.Tan(x), 2) / (x - 4);
                res = Rects(func, a, b, n, t);
                s1 = "Формула: tan(x)^2 / (x - 4)" + Environment.NewLine;
                s2 = "Результат: " + Convert.ToString(res) + Environment.NewLine;
            }
            else if (ind == 3)
            {
                double func(double x) => Math.Pow(x, 2);
                res = Rects(func, a, b, n, t);
                s1 = "Формула: x^2" + Environment.NewLine;
                s2 = "Результат: " + Convert.ToString(res) + Environment.NewLine;
            }
            else if (ind == 4)
            {
                double func(double x) => Math.Pow(x, 3);
                res = Rects(func, a, b, n, t);
                s1 = "Формула: x^3" + Environment.NewLine;
                s2 = "Результат: " + Convert.ToString(res) + Environment.NewLine;
            }
            else if (ind == 5)
            {
                double func(double x) => x / (x - 1);
                res = Rects(func, a, b, n, t);
                s1 = "Формула: x / (x - 1)" + Environment.NewLine;
                s2 = "Результат: " + Convert.ToString(res) + Environment.NewLine;
            }
            else if (ind == 6)
            {
                double func(double x) => x * (12 + x);
                res = Rects(func, a, b, n, t);
                s1 = "x * (12 + x)" + Environment.NewLine;
                s2 = "Результат: " + Convert.ToString(res) + Environment.NewLine;
            }
            return s1 + s2;
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            try
            {
                double a = float.Parse(LeftLim.Text);
                double b = float.Parse(RightLim.Text);
                int ind = FormulBox.SelectedIndex;
                int meth;
                string outb = "";
                OutBox.Clear();
                if ((a == 0) && (b == 0))
                {
                    MessageBox.Show("Два отрезка не могут быть равны нулю!\n" +
                        "Хотя бы один должен иметь значение отличное от него!\n");

                }
                else
                {
                    int n = int.Parse(NumOtr.Text);
                    if (n < 2)
                    {
                        MessageBox.Show("Укажите хотя бы две точки для деления отрезка");
                    }
                    else
                    {                                              
                        if(leftRekt.Checked)
                        {
                            meth = 0;
                            outb = Solvation(ind, a, b, n, meth);
                        }
                        else if (rightRekt.Checked)
                        {
                            meth = 1;
                            outb = Solvation(ind, a, b, n, meth);
                        }
                        else if (centralRekt.Checked)
                        {
                            meth = 2;
                            outb = Solvation(ind, a, b, n, meth);
                        }
                        OutBox.AppendText(outb);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Проверьте правильность написания формулы.");
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            FormulBox.SelectedIndex = 0;
        }

        private void AboutProg_Click(object sender, EventArgs e)
        {
            AboutForm a = new AboutForm();
            a.ShowDialog();
        }

        private void AboutMeth_Click(object sender, EventArgs e)
        {
            AboutMethod a = new AboutMethod();
            a.ShowDialog();
        }

        private void HelpMe_Click(object sender, EventArgs e)
        {
            Help a = new Help();
            a.ShowDialog();
        }
    }
}
