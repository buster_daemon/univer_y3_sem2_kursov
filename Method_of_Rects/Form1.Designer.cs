﻿namespace Method_of_Rects
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.LeftLim = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.RightLim = new System.Windows.Forms.TextBox();
            this.OutBox = new System.Windows.Forms.TextBox();
            this.NumOtr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.StartButton = new System.Windows.Forms.Button();
            this.Method = new System.Windows.Forms.GroupBox();
            this.centralRekt = new System.Windows.Forms.RadioButton();
            this.rightRekt = new System.Windows.Forms.RadioButton();
            this.leftRekt = new System.Windows.Forms.RadioButton();
            this.FormulBox = new System.Windows.Forms.ComboBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.AboutProg = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutMeth = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpMe = new System.Windows.Forms.ToolStripMenuItem();
            this.Method.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Левый предел:";
            // 
            // LeftLim
            // 
            this.LeftLim.Location = new System.Drawing.Point(137, 28);
            this.LeftLim.MaxLength = 3;
            this.LeftLim.Name = "LeftLim";
            this.LeftLim.Size = new System.Drawing.Size(100, 20);
            this.LeftLim.TabIndex = 1;
            this.LeftLim.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Правый предел:";
            // 
            // RightLim
            // 
            this.RightLim.Location = new System.Drawing.Point(137, 54);
            this.RightLim.MaxLength = 3;
            this.RightLim.Name = "RightLim";
            this.RightLim.Size = new System.Drawing.Size(100, 20);
            this.RightLim.TabIndex = 3;
            this.RightLim.Text = "0";
            // 
            // OutBox
            // 
            this.OutBox.Location = new System.Drawing.Point(243, 28);
            this.OutBox.Multiline = true;
            this.OutBox.Name = "OutBox";
            this.OutBox.ReadOnly = true;
            this.OutBox.Size = new System.Drawing.Size(545, 432);
            this.OutBox.TabIndex = 4;
            this.OutBox.Text = "Вычисление интеграла методом прямоугольников.\r\nВыполнил Точнов Е., 2022 год.\r\nИсх" +
    "одный код: https://codeberg.org/buster_daemon/univer_y3_sem2_kursov";
            // 
            // NumOtr
            // 
            this.NumOtr.Location = new System.Drawing.Point(137, 80);
            this.NumOtr.MaxLength = 5;
            this.NumOtr.Name = "NumOtr";
            this.NumOtr.Size = new System.Drawing.Size(100, 20);
            this.NumOtr.TabIndex = 5;
            this.NumOtr.Text = "2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Количество отрезков:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Введите формулу:";
            // 
            // ClearBtn
            // 
            this.ClearBtn.AutoSize = true;
            this.ClearBtn.Location = new System.Drawing.Point(138, 238);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(99, 23);
            this.ClearBtn.TabIndex = 9;
            this.ClearBtn.Text = "Очистить вывод";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(15, 238);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(75, 23);
            this.StartButton.TabIndex = 10;
            this.StartButton.Text = "Решить";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // Method
            // 
            this.Method.Controls.Add(this.centralRekt);
            this.Method.Controls.Add(this.rightRekt);
            this.Method.Controls.Add(this.leftRekt);
            this.Method.Location = new System.Drawing.Point(15, 132);
            this.Method.Name = "Method";
            this.Method.Size = new System.Drawing.Size(222, 100);
            this.Method.TabIndex = 13;
            this.Method.TabStop = false;
            this.Method.Text = "Метод решения";
            // 
            // centralRekt
            // 
            this.centralRekt.AutoSize = true;
            this.centralRekt.Location = new System.Drawing.Point(6, 65);
            this.centralRekt.Name = "centralRekt";
            this.centralRekt.Size = new System.Drawing.Size(155, 17);
            this.centralRekt.TabIndex = 2;
            this.centralRekt.Text = "Средние прямоугольники";
            this.centralRekt.UseVisualStyleBackColor = true;
            // 
            // rightRekt
            // 
            this.rightRekt.AutoSize = true;
            this.rightRekt.Location = new System.Drawing.Point(6, 42);
            this.rightRekt.Name = "rightRekt";
            this.rightRekt.Size = new System.Drawing.Size(152, 17);
            this.rightRekt.TabIndex = 1;
            this.rightRekt.Text = "Правые прямоугольники";
            this.rightRekt.UseVisualStyleBackColor = true;
            // 
            // leftRekt
            // 
            this.leftRekt.AutoSize = true;
            this.leftRekt.Checked = true;
            this.leftRekt.Location = new System.Drawing.Point(6, 19);
            this.leftRekt.Name = "leftRekt";
            this.leftRekt.Size = new System.Drawing.Size(146, 17);
            this.leftRekt.TabIndex = 0;
            this.leftRekt.TabStop = true;
            this.leftRekt.Text = "Левые прямоугольники";
            this.leftRekt.UseVisualStyleBackColor = true;
            // 
            // FormulBox
            // 
            this.FormulBox.AllowDrop = true;
            this.FormulBox.FormattingEnabled = true;
            this.FormulBox.Items.AddRange(new object[] {
            "sin(x)",
            "cos(x) + sin(x)",
            "tg(x)^2 / (x * 4)",
            "x^2",
            "x^3",
            "x / (x - 1)",
            "x * (12 + x)"});
            this.FormulBox.Location = new System.Drawing.Point(137, 109);
            this.FormulBox.Name = "FormulBox";
            this.FormulBox.Size = new System.Drawing.Size(100, 21);
            this.FormulBox.TabIndex = 14;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 15;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AboutProg,
            this.AboutMeth,
            this.HelpMe});
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(52, 22);
            this.toolStripButton1.Text = "Инфо";
            // 
            // AboutProg
            // 
            this.AboutProg.Name = "AboutProg";
            this.AboutProg.Size = new System.Drawing.Size(180, 22);
            this.AboutProg.Text = "О программе";
            this.AboutProg.Click += new System.EventHandler(this.AboutProg_Click);
            // 
            // AboutMeth
            // 
            this.AboutMeth.Name = "AboutMeth";
            this.AboutMeth.Size = new System.Drawing.Size(180, 22);
            this.AboutMeth.Text = "О методе решения";
            this.AboutMeth.Click += new System.EventHandler(this.AboutMeth_Click);
            // 
            // HelpMe
            // 
            this.HelpMe.Name = "HelpMe";
            this.HelpMe.Size = new System.Drawing.Size(180, 22);
            this.HelpMe.Text = "Справка";
            this.HelpMe.Click += new System.EventHandler(this.HelpMe_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 478);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.FormulBox);
            this.Controls.Add(this.Method);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.ClearBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NumOtr);
            this.Controls.Add(this.OutBox);
            this.Controls.Add(this.RightLim);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LeftLim);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Метод прямоугольников";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Method.ResumeLayout(false);
            this.Method.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LeftLim;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox RightLim;
        private System.Windows.Forms.TextBox OutBox;
        private System.Windows.Forms.TextBox NumOtr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.GroupBox Method;
        private System.Windows.Forms.RadioButton centralRekt;
        private System.Windows.Forms.RadioButton rightRekt;
        private System.Windows.Forms.RadioButton leftRekt;
        private System.Windows.Forms.ComboBox FormulBox;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem AboutProg;
        private System.Windows.Forms.ToolStripMenuItem AboutMeth;
        private System.Windows.Forms.ToolStripMenuItem HelpMe;
    }
}

