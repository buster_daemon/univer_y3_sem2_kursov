﻿using System;
using System.Windows.Forms;

namespace Method_of_Rects
{
    public partial class AboutMethod : Form
    {
        public AboutMethod()
        {
            InitializeComponent();
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
